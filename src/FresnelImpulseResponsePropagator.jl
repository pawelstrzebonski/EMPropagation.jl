import AbstractFFTs

"""
    FresnelImpulseResponsePropagator(u1, dx1[, dy1], wavelength, z)->(u2, x2[, y2])

Use Fresnel impulse response propagation to propagate a field.

# Arguments
- `u1`: near-field.
- `dx1::Union{Number, AbstractVector}`: near-field sampling.
- `dy1::Union{Number, AbstractVector}`: near-field sampling.
- `wavelength::Number`: wavelength of light.
- `z::Number`: propagation distance.

Use Fresnel impulse response propagation to propagate the near-field
array `u1` and near-field sample spacing `dx1` (and `dy1` for 2D)
(which may be either scalars for uniform sampling, or vectors for
non-uniform sampling) at a wavelength `wavelength`, calculate the 
uniformly sampled field `u2` at a distance `z`. Returns the side-length
of the field `u2` as `Lx2` (and `Ly2` for 2D).
"""
function FresnelImpulseResponsePropagator(
    u1::AbstractVector,
    dx1,
    wavelength::Number,
    z::Number,
)
    @assert size(dx1) == () || length(dx1) == length(u1)
    A = typeof(u1)
    k = 2 * pi / wavelength
    M = length(u1)
    dx1av = mean(dx1)
    Lx1 = M * dx1av
    X1 = A(ffrange(M, Lx1))
    dxdy, R2 = dx1av, X1 .^ 2
    h = 1 / (im * wavelength * z) * exp.((im * k / (2 * z)) .* (R2))
    H = xfft(dx1av, h) .* dxdy
    U1 = xfft(dx1, u1)
    U2 = H .* U1
    u2 = AbstractFFTs.ifftshift(AbstractFFTs.ifft(U2))
    u2, ffrange(M, Lx1)
end
function FresnelImpulseResponsePropagator(
    u1::AbstractMatrix,
    dx1,
    dy1,
    wavelength::Number,
    z::Number,
)
    @assert (size(dx1) == () && size(dy1) == ()) || (length(dx1), length(dy1)) == size(u1)
    A = typeof(u1)
    k = 2 * pi / wavelength
    M, N = size(u1)
    dx1av, dy1av = mean(dx1), mean(dy1)
    Lx1, Ly1 = M * dx1av, N * dy1av
    x1, y1 = ffrange(M, Lx1), ffrange(N, Ly1)
    X1, Y1 = meshgrid(x1 .^ 2, y1 .^ 2)
    dxdy, R2 = dx1av * dy1av, A(X1 .+ Y1)
    h = 1 / (im * wavelength * z) * exp.((im * k / (2 * z)) .* (R2))
    H = xfft(dx1av, dy1av, h) .* dxdy
    U1 = xfft(dx1, dy1, u1)
    U2 = H .* U1
    u2 = AbstractFFTs.ifftshift(AbstractFFTs.ifft(U2))
    u2, ffrange(M, Lx1), ffrange(N, Ly1)
end

"""
    FresnelImpulseResponsePropagatorFun(u1, dx1[, dy1], wavelength, z)

Returns a function `f(x, z)=FresnelImpulseResponsePropagator(x, dx1[, dy1], wavelength, z)`
using pre-calculated constants and pre-planned FFTs.
"""
function FresnelImpulseResponsePropagatorFun(
    u1::AbstractVector,
    dx1,
    wavelength::Number,
    z::Number,
)
    @assert size(dx1) == () || length(dx1) == length(u1)
    A = typeof(u1)
    k = 2 * pi / wavelength
    M = length(u1)
    dx1av = mean(dx1)
    Lx1 = M * dx1av
    X1 = A(ffrange(M, Lx1))
    dxdy, R2 = dx1av, X1 .^ 2
    h = 1 / (im * wavelength * z) * exp.((im * k / (2 * z)) .* (R2))
    H = xfft(dx1av, h) .* dxdy
    U1 = xfft(dx1, u1)
    U2 = H .* U1
    #u2 = AbstractFFTs.fftshift(AbstractFFTs.ifft(U2))
    xfftph = xfftfun(dx1av, h)
    xfftp = xfftfun(dx1, u1)
    ifftp = AbstractFFTs.plan_ifft(U2)
    (x, z) -> (
        AbstractFFTs.ifftshift(
            ifftp * (
                xfftph(1 / (im * wavelength * z) * exp.((im * k / (2 * z)) .* (R2))) .*
                dxdy .* xfftp(x)
            ),
        ),
        ffrange(M, Lx1),
    )
end
function FresnelImpulseResponsePropagatorFun(
    u1::AbstractMatrix,
    dx1,
    dy1,
    wavelength::Number,
    z::Number,
)
    @assert (size(dx1) == () && size(dy1) == ()) || (length(dx1), length(dy1)) == size(u1)
    A = typeof(u1)
    k = 2 * pi / wavelength
    M, N = size(u1)
    dx1av, dy1av = mean(dx1), mean(dy1)
    Lx1, Ly1 = M * dx1av, N * dy1av
    x1, y1 = ffrange(M, Lx1), ffrange(N, Ly1)
    X1, Y1 = meshgrid(x1 .^ 2, y1 .^ 2)
    dxdy, R2 = dx1av * dy1av, A(X1 .+ Y1)
    h = 1 / (im * wavelength * z) * exp.((im * k / (2 * z)) .* (R2))
    H = xfft(dx1av, dy1av, h) .* dxdy
    U1 = xfft(dx1, dy1, u1)
    U2 = H .* U1
    #u2 = AbstractFFTs.ifftshift(AbstractFFTs.ifft(U2))
    xfftph = xfftfun(dx1av, dy1av, h)
    xfftp = xfftfun(dx1, dy1, u1)
    ifftp = AbstractFFTs.plan_ifft(U2)
    (x, z) -> (
        AbstractFFTs.ifftshift(
            ifftp * (
                xfftph(1 / (im * wavelength * z) * exp.((im * k / (2 * z)) .* (R2))) .*
                dxdy .* xfftp(x)
            ),
        ),
        ffrange(M, Lx1),
        ffrange(N, Ly1),
    )
end
