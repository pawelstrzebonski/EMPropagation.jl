module EMPropagation

include("FresnelTransferFunctionPropagator.jl")
include("FresnelImpulseResponsePropagator.jl")
include("FraunhoferPropagator.jl")
include("nudft.jl")
include("utils.jl")
include("AutomaticPropagators.jl")

#TODO: Verify that these are correct (that non-rectangular and 1D are correct)
export FraunhoferPropagator,
    FresnelTransferFunctionPropagator,
    FresnelImpulseResponsePropagator,
    FraunhoferPropagatorFun,
    FresnelTransferFunctionPropagatorFun,
    FresnelImpulseResponsePropagatorFun,
    criticalz,
    zeropad,
    autofarfield

end # module
