import NFFT

"""
    NUDFT(dx[, dy], u)

Calculate the FFT of the non-uniformly sampled array `u` with sample
spacing `dx` (and `dy` for 2D). 
"""
function NUDFT(dx::AbstractVector, u::AbstractVector)
    @assert size(dx) == size(u)
    @warn "Using non-uniform FFT. This may give errors."
    x = cumsum(dx)
    x = (x ./ x[end]) .- (1 / 2)
    p = NFFT.plan_nfft(real.(x), length(u))
    U = adjoint(p) * complex.(u)
    # Shift the output to get zero-frequency component in the some location that normal FFT does
    circshift(U, div(length(dx) + 1, 2))
end
function NUDFT(dx::AbstractVector, dy::AbstractVector, u::AbstractMatrix)
    @assert (length(dx), length(dy)) == size(u)
    @warn "Using non-uniform FFT. This may give errors."
    x = cumsum(dx)
    x = (x ./ x[end]) .- (1 / 2)
    y = cumsum(dy)
    y = (y ./ y[end]) .- (1 / 2)
    xy = [(x*ones(size(y))')[:] (ones(size(x))*y')[:]]
    p = NFFT.plan_nfft(real.(xy'), size(u))
    U = adjoint(p) * complex.(u[:])
    # Shift the output to get zero-frequency component in the some location that normal FFT does
    circshift(reshape(U, size(u)), (div(length(dx) + 1, 2), div(length(dy) + 1, 2)))
end

"""
    NUDFTfun(dx[, dy], u)

Returns a function `f(x)=NUDFT(dx[, dy], x)` with pre-planned NFFT calls.
"""
function NUDFTfun(dx::AbstractVector, u::AbstractVector)
    @assert size(dx) == size(u)
    @warn "Using non-uniform FFT. This may give errors."
    x = cumsum(dx)
    x = (x ./ x[end]) .- (1 / 2)
    p = NFFT.plan_nfft(real.(x), length(u))
    #U = NFFT.nfft(p, complex.(u))
    # Shift the output to get zero-frequency component in the some location that normal FFT does
    x -> circshift(adjoint(p) * complex.(x), div(length(dx) + 1, 2))
end
function NUDFTfun(dx::AbstractVector, dy::AbstractVector, u::AbstractMatrix)
    @assert (length(dx), length(dy)) == size(u)
    @warn "Using non-uniform FFT. This may give errors."
    x = cumsum(dx)
    x = (x ./ x[end]) .- (1 / 2)
    y = cumsum(dy)
    y = (y ./ y[end]) .- (1 / 2)
    xy = [(x*ones(size(y))')[:] (ones(size(x))*y')[:]]
    p = NFFT.plan_nfft(real.(xy'), size(u))
    #U = NFFT.nfft(p, complex.(u))
    # Shift the output to get zero-frequency component in the some location that normal FFT does
    x -> circshift(
        reshape(adjoint(p) * complex.(x[:]), size(u)),
        (div(length(dx) + 1, 2), div(length(dy) + 1, 2)),
    )
end
