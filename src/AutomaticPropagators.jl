"""
    autofarfield(dx[, dy], u1, wavelength, NZP)->(thetax[, thetay], ff)

Propagate the given near-field into the far-field using Fraunhofer
propagation.

# Arguments
- `u1`: near-field.
- `dx::Union{Number, AbstractVector}`: near-field sampling.
- `dy::Union{Number, AbstractVector}`: near-field sampling.
- `wavelength::Number`: wavelength of light.
- `NZP::Integer`: Number of samples to zero-pad on each edge of near-field.

Given near-field array `u1` and sample spacing `dx` (and `dy` for 2D)
(which may be either scalars for uniform sampling, or vectors for
non-uniform sampling) at a wavelength `wavelength`, calculate the far-field
`ff` at far-field angles `thetax` (and `thetay` for 2D) (where `thetax`
and `thetay` are in radians).

`NZP` determines the number of zero-padding to apply to the near-field
before propagating. Larger zero-padding enables greater resolution in
the far-field, at a computational cost.
"""
function autofarfield(dx, dy, u1::AbstractMatrix, wavelength::Number, NZP::Integer)
    @assert (size(dx) == () && size(dy) == ()) || (length(dx), length(dy)) == size(u1)
    dxave, dyave = mean(dx), mean(dy)
    nf = zeropad(u1, NZP)
    dxnf = size(dx) == () ? dx : [fill(dxave, NZP); dx; fill(dxave, NZP)]
    dynf = size(dy) == () ? dy : [fill(dyave, NZP); dy; fill(dyave, NZP)]

    #TODO: This does not work / is slow on GPUArrays (?) due to cumsum using scalar operations
    #=
    # Determine whether Fraunhofer propagation will yield reasonable phase
    w = beamwidth(dxnf, dynf, nf) ./ 2
    # Determine the sampling for Fraunhofer propagation to have good phase
    Nfraunhofer = w .^ 2 ./ (dxave, dyave) .^ 2
    if all(size(nf) .< Nfraunhofer)
    @warn "Number of sampling points is likely too small for far-field phase to be believable. Consider increasing `NZP`."
    end
    =#

    z = criticalz(dyave, size(u1, 2), wavelength)
    ff, x2, y2 = FraunhoferPropagator(nf, dxnf, dynf, wavelength, z)
    thetax = atan.(x2, real.(z))
    thetay = atan.(y2, real.(z))
    thetax, thetay, ff
end
function autofarfield(dx, u1::AbstractVector, wavelength::Number, NZP::Integer)
    @assert size(dx) == () || length(dx) == length(u1)
    dxave = mean(dx)
    nf = zeropad(u1, NZP)
    dxnf = size(dx) == () ? dx : [fill(dxave, NZP); dx; fill(dxave, NZP)]

    #TODO: This does not work / is slow on GPUArrays (?) due to cumsum using scalar operations
    #=
    # Determine whether Fraunhofer propagation will yield reasonable phase
    w = beamwidth(dxnf, nf) / 2
    # Determine the sampling for Fraunhofer propagation to have good phase
    Nfraunhofer = w ^ 2 / dxave ^ 2
    if length(nf) < Nfraunhofer
    @warn "Number of sampling points is likely too small for far-field phase to be believable. Consider increasing `NZP`."
    end
    =#

    z = criticalz(dxave, length(u1), wavelength)
    ff, x2 = FraunhoferPropagator(nf, dxnf, wavelength, z)
    thetax = atan.(x2, real.(z))
    thetax, ff
end

#TODO: Check these to ensure they're reasonable
#TODO: Implement non-uniform forms
"""
    propagator_suggest(dx, nf, wavelength, z)->PropagatorFunction

Analyses various aspects of the propagation problem such as sampling,
Fresnel Number, and bandwidth criteria to make suggestions as to which
propagator to use, and any modifications to the problem that can/should
be made. It returns the most suggested propagator. Please take the results
with a grain of salt, and ideally consult section 5.4 of the "Computational
Fourier Optics" reference.
"""
function propagator_suggest(dx::Number, nf::AbstractVector, wavelength::Number, z::Number)
    @warn "This function makes various suggestions based on various parameters, some mutually conflicting."
    Ninput = length(nf)
    L = Ninput * dx

    # Calculate beamwidth and bandwidth
    w = beamwidth(dx, nf) / 2
    B1 = beamwidth(dx / Ninput, AbstractFFTs.fftshift(xfft(dx, nf))) / 2
    # Assume effective source width to be twice beamwidth
    D1 = 2 * w

    # Support is recommended to be such that about/between L=2*D1 and L=3*D1
    isBigEnough = L >= 2 * D1
    Nbig = round(Int, 2 * D1 / dx)

    # Consider the Fresnel Number to determine which regime it's in
    FresnelNumber = w^2 / (wavelength * z)
    isFraunhoferRegime = FresnelNumber <= 1 / 10
    isFresnelRegime = FresnelNumber <= 1

    # Calculate the bandwidth limits for the propagators and check if bandwidth criteria are satisfied
    Shortbw = 1 / (2 * dx)
    Longbw = L / (2 * wavelength * z)
    isShortBandwidth = B1 <= Shortbw
    shortBWdx = 1 / (2 * B1)
    isLongBandwidth = B1 <= Longbw
    longBWN = round(Int, 2 * z * wavelength / dx)

    # Determine the sampling for Fraunhofer propagation to have good phase
    Nfraunhofer = round(Int, w^2 / dx^2)
    isFraunhoferSize = Ninput >= Nfraunhofer

    # Consider the distance regimes
    isShortDistance = dx > wavelength * z / L
    isLongDistance = dx < wavelength * z / L
    Ncritical = round(Int, wavelength * z / dx^2)
    dxcritical = wavelength * z / L

    if !isBigEnough
        @warn "Field source size is rather large compared to array size. Consider zero-padding to N>=" *
              string(Nbig)
    end

    TF, IR, FF = 0, 0, 0

    if isFraunhoferRegime
        FF += 1
        @info "We're in the Fraunhofer regime. Suggest `FraunhoferPropagator`"
        if !isFraunhoferSize
            @warn "Insufficient samples for Fraunhofer phase to be believable. Consider zero-padding to N>>" *
                  string(Nfraunhofer)
        end
    elseif isFresnelRegime
        TF += 1
        IR += 1
        @info "We're in the Fresnel regime. Suggest either `FresnelImpulseResponsePropagator` or `FresnelTransferFunctionPropagator`"
    else
        @info "We're outside of the Fresnel regime. Consider something else?"
    end

    if isShortDistance
        TF += 1
        @info "We're in the short distance regime. Suggest `FresnelTransferFunctionPropagator`. Consider decreasing `dx` to =" *
              string(dxcritical)
        if !isShortBandwidth
            @warn "Short distance bandwidth criterion violated. Consider decreasing `dx` to <=" *
                  string(shortBWdx)
        end
    elseif isLongDistance
        @info "We're in the Long distance regime. Consider zero-padding field to N=" *
              string(Ncritical)
        if isLongBandwidth
            TF += 1
            @info "Long distance bandwidth criterion satisfied. Suggest `FresnelTransferFunctionPropagator`."
        else
            IR += 1
            @warn "Long distance bandwidth criterion violated. Suggest `FresnelImpulseResponsePropagator`. Consider zero-padding field to N>=" *
                  string(longBWN)
        end
    else
        TF += 1
        IR += 1
        @info "We're critically sampling. Suggest either `FresnelImpulseResponsePropagator` or `FresnelTransferFunctionPropagator`"
    end
    (
        FraunhoferPropagator,
        FresnelTransferFunctionPropagator,
        FresnelImpulseResponsePropagator,
    )[findmax([FF, TF, IR])[2]]
end
function propagator_suggest(
    dx::Number,
    dy::Number,
    nf::AbstractMatrix,
    wavelength::Number,
    z::Number,
)
    @warn "This function makes various suggestions based on various parameters, some mutually conflicting."
    @warn "This function is very much a work in progress."
    Ninput, Minput = size(nf)
    L = (Ninput * dx, Minput * dy)

    # Calculate beamwidth and bandwidth
    w = beamwidth(dx, dy, nf) ./ 2
    B1 = beamwidth(dx / Ninput, dy / Minput, AbstractFFTs.fftshift(xfft(dx, dy, nf))) ./ 2
    # Assume effective source width to be twice beamwidth
    D1 = 2 .* w

    # Support is recommended to be such that about/between L=2*D1 and L=3*D1
    isBigEnough = all(L .>= 2 .* D1)
    Nbig = round.(Int, 2 .* D1 ./ (dx, dy))

    # Consider the Fresnel Number to determine which regime it's in
    #TODO: assume Fresnel number dominated by largest beamwidth parameter
    FresnelNumber = w .^ 2 ./ (wavelength * z)
    isFraunhoferRegime = maximum(FresnelNumber) <= 1 / 10
    isFresnelRegime = maximum(FresnelNumber) <= 1

    # Calculate the bandwidth limits for the propagators and check if bandwidth criteria are satisfied
    Shortbw = (1 / (2 * dx), 1 / (2 * dy))
    Longbw = L ./ (2 * wavelength * z)
    isShortBandwidth = all(B1 .<= Shortbw)
    shortBWdx = 1 ./ (2 .* B1)
    isLongBandwidth = all(B1 .<= Longbw)
    longBWN = round.(Int, 2 * z * wavelength ./ (dx, dy))

    # Determine the sampling for Fraunhofer propagation to have good phase
    Nfraunhofer = round.(Int, w .^ 2 ./ (dx, dy) .^ 2)
    isFraunhoferSize = all(Ninput .>= Nfraunhofer)

    # Consider the distance regimes
    isShortDistance = (dx, dy) .> wavelength * z ./ L
    isLongDistance = (dx, dy) .< wavelength * z ./ L
    Ncritical = round.(Int, wavelength * z ./ (dx, dy) .^ 2)
    dxcritical = wavelength * z ./ L

    if !isBigEnough
        @warn "Field source size is rather large compared to array size. Consider zero-padding to N>=" *
              string(Nbig)
    end

    TF, IR, FF = 0, 0, 0

    if isFraunhoferRegime
        @info "We're in the Fraunhofer regime. Suggest `FraunhoferPropagator`"
        FF += 1
        if !isFraunhoferSize
            @warn "Insufficient samples for Fraunhofer phase to be believable. Consider zero-padding to N>>" *
                  string(Nfraunhofer)
        end
    elseif isFresnelRegime
        TF += 1
        IR += 1
        @info "We're in the Fresnel regime. Suggest either `FresnelImpulseResponsePropagator` or `FresnelTransferFunctionPropagator`"
    else
        @info "We're outside of the Fresnel regime. Consider something else?"
    end

    if all(isShortDistance)
        TF += 1
        @info "We're in the short distance regime. Suggest `FresnelTransferFunctionPropagator`. Consider decreasing `dx` to =" *
              string(dxcritical)
        if !isShortBandwidth
            @warn "Short distance bandwidth criterion violated. Consider decreasing `dx` to <=" *
                  string(shortBWdx)
        end
    elseif all(isLongDistance)
        @info "We're in the Long distance regime. Consider zero-padding field to N=" *
              string(Ncritical)
        if isLongBandwidth
            TF += 1
            @info "Long distance bandwidth criterion satisfied. Suggest `FresnelTransferFunctionPropagator`."
        else
            IR += 1
            @warn "Long distance bandwidth criterion violated. Suggest `FresnelImpulseResponsePropagator`. Consider zero-padding field to N>=" *
                  string(longBWN)
        end
    elseif none(isShortDistance) && none(isLongDistance)
        TF += 1
        IR += 1
        @info "We're critically sampling. Suggest either `FresnelImpulseResponsePropagator` or `FresnelTransferFunctionPropagator`"
    end
    (
        FraunhoferPropagator,
        FresnelTransferFunctionPropagator,
        FresnelImpulseResponsePropagator,
    )[findmax([FF, TF, IR])[2]]
end
