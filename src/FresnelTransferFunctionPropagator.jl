import AbstractFFTs

"""
    FresnelTransferFunctionPropagator(u1, dx1[, dy1], wavelength, z)->(u2, x2[, y2])

Use Fresnel transfer function propagation to propagate a field.

# Arguments
- `u1`: near-field.
- `dx1::Union{Number, AbstractVector}`: near-field sampling.
- `dy1::Union{Number, AbstractVector}`: near-field sampling.
- `wavelength::Number`: wavelength of light.
- `z::Number`: propagation distance.

Use Fresnel transfer function propagation to propagate the near-field
array `u1` and near-field sample spacing `dx1` (and `dy1` for 2D)
(which may be either scalars for uniform sampling, or vectors for
non-uniform sampling) at a wavelength `wavelength`, calculate the 
uniformly sampled field `u2` at a distance `z`. Returns the coordinates
of the field `u2` as `x2` (and `y2` for 2D).
"""
function FresnelTransferFunctionPropagator(
    u1::AbstractVector,
    dx1,
    wavelength::Number,
    z::Number,
)
    @assert size(dx1) == () || length(dx1) == length(u1)
    A = typeof(u1)
    M = length(u1)
    dx1av = mean(dx1)
    FX2 = A(ffrange(M, 1 / dx1av))
    FR2 = FX2 .^ 2
    H = AbstractFFTs.fftshift(@. exp(-im * pi * wavelength * z * FR2))
    U1 = xfft(dx1, u1)
    U2 = H .* U1
    u2 = AbstractFFTs.ifftshift(AbstractFFTs.ifft(U2))
    u2, ffrange(M, dx1av * M)
end
function FresnelTransferFunctionPropagator(
    u1::AbstractMatrix,
    dx1,
    dy1,
    wavelength::Number,
    z::Number,
)
    @assert (size(dx1) == () && size(dy1) == ()) || (length(dx1), length(dy1)) == size(u1)
    A = typeof(u1)
    M, N = size(u1)
    dx1av, dy1av = mean(dx1), mean(dy1)
    Fx2, Fy2 = ffrange(M, 1 / dx1av), ffrange(N, 1 / dy1av)
    FX2, FY2 = meshgrid(Fx2 .^ 2, Fy2 .^ 2)
    FR2 = A(FX2 .+ FY2)
    H = AbstractFFTs.fftshift(@. exp(-im * pi * wavelength * z * FR2))
    U1 = xfft(dx1, dy1, u1)
    U2 = H .* U1
    u2 = AbstractFFTs.ifftshift(AbstractFFTs.ifft(U2))
    u2, ffrange(M, dx1av * M), ffrange(N, dy1av * N)
end

"""
    FresnelTransferFunctionPropagatorFun(u1, dx1[, dy1], wavelength, z)

Returns a function `f(x, z)=FresnelTransferFunctionPropagator(x, dx1[, dy1], wavelength, z)`
using pre-calculated constants and pre-planned FFTs.
"""
function FresnelTransferFunctionPropagatorFun(
    u1::AbstractVector,
    dx1,
    wavelength::Number,
    z::Number,
)
    @assert size(dx1) == () || length(dx1) == length(u1)
    A = typeof(u1)
    M = length(u1)
    dx1av = mean(dx1)
    FX2 = A(ffrange(M, 1 / dx1av))
    FR2 = FX2 .^ 2
    H = AbstractFFTs.fftshift(@. exp(-im * pi * wavelength * z * FR2))
    U1 = xfft(dx1, u1)
    U2 = H .* U1
    #u2 = AbstractFFTs.ifftshift(AbstractFFTs.ifft(U2))
    xfftp = xfftfun(dx1, u1)
    ifftp = AbstractFFTs.plan_ifft(U2)
    (x, z) -> (
        AbstractFFTs.ifftshift(
            ifftp * (
                AbstractFFTs.fftshift(@. exp(-im * pi * wavelength * z * FR2)) .* xfftp(x)
            ),
        ),
        ffrange(M, dx1av * M),
    )
end
function FresnelTransferFunctionPropagatorFun(
    u1::AbstractMatrix,
    dx1,
    dy1,
    wavelength::Number,
    z::Number,
)
    @assert (size(dx1) == () && size(dy1) == ()) || (length(dx1), length(dy1)) == size(u1)
    A = typeof(u1)
    M, N = size(u1)
    dx1av, dy1av = mean(dx1), mean(dy1)
    Fx2, Fy2 = ffrange(M, 1 / dx1av), ffrange(N, 1 / dy1av)
    FX2, FY2 = meshgrid(Fx2 .^ 2, Fy2 .^ 2)
    FR2 = A(FX2 .+ FY2)
    H = AbstractFFTs.fftshift(@. exp(-im * pi * wavelength * z * FR2))
    U1 = xfft(dx1, dy1, u1)
    U2 = H .* U1
    #u2 = AbstractFFTs.ifftshift(AbstractFFTs.ifft(U2))
    xfftp = xfftfun(dx1, dy1, u1)
    ifftp = AbstractFFTs.plan_ifft(U2)
    (x, z) -> (
        AbstractFFTs.ifftshift(
            ifftp * (
                AbstractFFTs.fftshift(@. exp(-im * pi * wavelength * z * FR2)) .* xfftp(x)
            ),
        ),
        ffrange(M, dx1av * M),
        ffrange(N, dy1av * N),
    )
end
