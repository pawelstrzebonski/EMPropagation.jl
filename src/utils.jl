import AbstractFFTs

"""
    zeropad(A, NZP)

Zero-pad array `A` with `NZP` many zeros around each edge.
"""
function zeropad(A::AbstractMatrix, NZP::Integer)
    M, P = size(A)
    A2 = typeof(A)(zeros(eltype(A), 2 * NZP + M, 2 * NZP + P))
    A2[NZP.+(1:M), NZP.+(1:P)] .= A
    A2
end

function zeropad(A::AbstractVector, NZP::Integer)
    A2 = typeof(A)(zeros(eltype(A), 2 * NZP + length(A)))
    A2[NZP.+(1:length(A))] .= A
    A2
end

"""
    meshgrid(x, y)->(X, Y)

Given vectors `x` and `y` of x and y coordinates, return matrices `X` and `Y`
corresponding to a full 2-D grid.
"""
meshgrid(x::AbstractVector, y::AbstractVector) = x * ones(size(y))', ones(size(x)) * y'

"""
    criticalz(dx, m, lambda)

Calculate the propagation distance for critical sampling given near-field
sample spacing `dx`, sampling points `m`, and wavelength `lambda`.
"""
criticalz(dx::Number, m::Number, lambda::Number) = (dx^2 * m / lambda)

"""
    mean(x)

Arithmetic average
"""
mean(x::Number) = x
mean(x::AbstractArray) = sum(x) / length(x[:])

"""
    xfft(dx[, dy], u)

Wrapper around FFT functions that runs the appropriate FFT code.
"""
function xfft(dx::Number, u::AbstractVector)
    #AbstractFFTs.fft(AbstractFFTs.ifftshift(u))
    pf = AbstractFFTs.plan_fft(u)
    pf * AbstractFFTs.ifftshift(u)
end
function xfft(dx::AbstractVector, u::AbstractVector)
    NUDFT(AbstractFFTs.ifftshift(dx), AbstractFFTs.ifftshift(u))
end
function xfft(dx::Number, dy::Number, u::AbstractMatrix)
    #AbstractFFTs.fft(AbstractFFTs.ifftshift(u))
    pf = AbstractFFTs.plan_fft(u)
    pf * AbstractFFTs.ifftshift(u)
end
function xfft(dx::AbstractVector, dy::AbstractVector, u::AbstractMatrix)
    NUDFT(AbstractFFTs.ifftshift(dx), AbstractFFTs.ifftshift(dy), AbstractFFTs.ifftshift(u))
end

"""
    xfftfun(dx[, dy], u)

Returns a function `f(x)=xfft(dx[, dy], x)` with pre-planned FFT calls.
"""
function xfftfun(dx::Number, u::AbstractVector)
    #AbstractFFTs.fft(AbstractFFTs.ifftshift(u))
    pf = AbstractFFTs.plan_fft(u)
    x -> pf * AbstractFFTs.ifftshift(x)
end
function xfftfun(dx::AbstractVector, u::AbstractVector)
    NUDFTp = NUDFTfun(AbstractFFTs.ifftshift(dx), AbstractFFTs.ifftshift(u))
    x -> NUDFTp(AbstractFFTs.ifftshift(x))
end
function xfftfun(dx::Number, dy::Number, u::AbstractMatrix)
    #AbstractFFTs.fft(AbstractFFTs.ifftshift(u))
    pf = AbstractFFTs.plan_fft(u)
    x -> pf * AbstractFFTs.ifftshift(x)
end
function xfftfun(dx::AbstractVector, dy::AbstractVector, u::AbstractMatrix)
    NUDFTp = NUDFTfun(
        AbstractFFTs.ifftshift(dx),
        AbstractFFTs.ifftshift(dy),
        AbstractFFTs.ifftshift(u),
    )
    x -> NUDFTp(AbstractFFTs.ifftshift(x))
end

"""
    ffrange(N, L)

A range calculation utility to calculate the far-field grid points with
the correctly placed origin point.
"""
function ffrange(N::Integer, L::Number)
    L .* AbstractFFTs.fftshift(AbstractFFTs.fftfreq(N))
end

"""
    beamwidth(dx[, dy], nf)

Calculate the beamwidth as the width that contains 98% of the power.
"""
function beamwidth(dx, nf::AbstractVector)
    @assert size(dx) == () || length(dx) == length(nf)
    a = cumsum(abs2.(nf) .* dx) ./ sum(abs2.(nf) .* dx)
    sum(dx .* ((0.01 .<= a) .& (a .<= 0.99)))
end
function beamwidth(dx::Number, dy::Number, nf::AbstractMatrix)
    beamwidth(dx, reduce(+, abs.(nf), dims = 2)[:]),
    beamwidth(dy, reduce(+, abs.(nf), dims = 1)[:])
end
function beamwidth(dx::AbstractVector, dy::AbstractVector, nf::AbstractMatrix)
    @assert length(dx) == size(nf, 1) && length(dy) == size(nf, 2)
    beamwidth(dx, abs.(nf) * dy), beamwidth(dy, abs.(nf)' * dx)
end

"""
    none(x)

Returns true if none of `x` is `true`.
"""
none(x) = !any(x)
