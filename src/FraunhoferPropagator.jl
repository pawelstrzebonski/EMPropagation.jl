import AbstractFFTs

"""
    FraunhoferPropagator(u1, dx1[, dy1], wavelength, z)->(u2, x2[, y2])

Use Fraunhofer propagation to propagate a field.

# Arguments
- `u1`: near-field.
- `dx1::Union{Number, AbstractVector}`: near-field sampling.
- `dy1::Union{Number, AbstractVector}`: near-field sampling.
- `wavelength::Number`: wavelength of light.
- `z::Number`: propagation distance.

Use Fraunhofer propagation to propagate the near-field
array `u1` and near-field sample spacing `dx1` (and `dy1` for 2D)
(which may be either scalars for uniform sampling, or vectors for
non-uniform sampling) at a wavelength `wavelength`, calculate the 
uniformly sampled field `u2` at a distance `z`. Returns the coordinates
of the field `u2` as `x2` (and `y2` for 2D).
"""
function FraunhoferPropagator(u1::AbstractVector, dx1, wavelength::Number, z::Number)
    @assert size(dx1) == () || length(dx1) == length(u1)
    A = typeof(u1)
    M = length(u1)
    k = 2 * pi / wavelength
    dx1av = mean(dx1)
    Lx2 = wavelength * z / dx1av
    X2 = A(ffrange(M, Lx2))
    dxdy, R2 = dx1av, X2 .^ 2
    c = @. 1 / (im * wavelength * z) * exp(im * k / (2 * z) * R2) * dxdy
    u2 = c .* AbstractFFTs.fftshift(xfft(dx1, u1))
    u2, ffrange(M, Lx2)
end
function FraunhoferPropagator(u1::AbstractMatrix, dx1, dy1, wavelength::Number, z::Number)
    @assert (size(dx1) == () && size(dy1) == ()) || (length(dx1), length(dy1)) == size(u1)
    A = typeof(u1)
    k = 2 * pi / wavelength
    M, N = size(u1)
    dx1av, dy1av = mean(dx1), mean(dy1)
    Lx2, Ly2 = wavelength * z / dx1av, wavelength * z / dy1av
    x2, y2 = ffrange(M, Lx2), ffrange(N, Ly2)
    X2, Y2 = meshgrid(x2, y2)
    dxdy, R2 = dx1av * dy1av, A(X2 .^ 2 .+ Y2 .^ 2)
    c = @. 1 / (im * wavelength * z) * exp(im * k / (2 * z) * R2) * dxdy
    u2 = c .* AbstractFFTs.fftshift(xfft(dx1, dy1, u1))
    u2, ffrange(M, Lx2), ffrange(N, Ly2)
end

"""
    FraunhoferPropagatorFun(u1, dx1[, dy1], wavelength, z)

Returns a function `f(x, z)=FraunhoferPropagator(x, dx1[, dy1], wavelength, z)`
using pre-calculated constants and pre-planned FFTs.
"""
function FraunhoferPropagatorFun(u1::AbstractVector, dx1, wavelength::Number, z::Number)
    @assert size(dx1) == () || length(dx1) == length(u1)
    A = typeof(u1)
    M = length(u1)
    k = 2 * pi / wavelength
    dx1av = mean(dx1)
    Lx2 = wavelength / dx1av
    X2 = A(ffrange(M, Lx2))
    dxdy, R2 = dx1av, X2 .^ 2
    xfftp = xfftfun(dx1, u1)
    (x, z) -> (
        (@. 1 / (im * wavelength * z) * exp(im * k / (2 * z) * R2 * z^2) * dxdy) .*
        AbstractFFTs.fftshift(xfftp(x)),
        ffrange(M, Lx2 * z),
    )
end
function FraunhoferPropagatorFun(
    u1::AbstractMatrix,
    dx1,
    dy1,
    wavelength::Number,
    z::Number,
)
    @assert (size(dx1) == () && size(dy1) == ()) || (length(dx1), length(dy1)) == size(u1)
    A = typeof(u1)
    k = 2 * pi / wavelength
    M, N = size(u1)
    dx1av, dy1av = mean(dx1), mean(dy1)
    Lx2, Ly2 = wavelength / dx1av, wavelength / dy1av
    x2, y2 = ffrange(M, Lx2), ffrange(N, Ly2)
    X2, Y2 = meshgrid(x2, y2)
    dxdy, R2 = dx1av * dy1av, A(X2 .^ 2 .+ Y2 .^ 2)
    xfftp = xfftfun(dx1, dy1, u1)
    (x, z) -> (
        (@. 1 / (im * wavelength * z) * exp(im * k / (2 * z) * R2 * z^2) * dxdy) .*
        AbstractFFTs.fftshift(xfftp(x)),
        ffrange(M, Lx2 * z),
        ffrange(N, Ly2 * z),
    )
end
