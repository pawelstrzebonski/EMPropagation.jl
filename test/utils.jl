@testset "utils.jl" begin
    # Test zero-padding 1D array
    a = ones(10)
    azp = EMPropagation.zeropad(a, 5)
    @test length(azp) == 20
    @test all(isone.(azp[6:15]))
    @test all(iszero.(azp[1:5]))
    @test all(iszero.(azp[16:end]))

    # Test zero-padding 2D array
    a = ones(10, 5)
    azp = EMPropagation.zeropad(a, 5)
    @test size(azp) == (20, 15)
    @test all(isone.(azp[6:15, 6:10]))
    @test all(iszero.(azp[1:5, :]))
    @test all(iszero.(azp[16:end, :]))
    @test all(iszero.(azp[:, 1:5]))
    @test all(iszero.(azp[:, 11:15]))

    # Basic sanity tests of meshgrid
    x = 1:3
    y = 1:4
    X, Y = EMPropagation.meshgrid(x, y)
    @test size(X) == size(Y) == (length(x), length(y))
    @test X[:, 1] == x
    @test Y[1, :] == y

    # Simple test of critical sampling (based on table 5.2 (page 75) of
    # "Computational Fourier Optics")
    dx = 2e-3
    m = 250
    lambda = 0.5e-6
    z = EMPropagation.criticalz(dx, m, lambda)
    @test approxeq(z, 2000)

    # Simple tests of mean
    @test EMPropagation.mean(1:10) == 5.5
    @test EMPropagation.mean(ones(2, 3)) == 1

    @test EMPropagation.ffrange(4, 2) == -1:0.5:0.5
    @test EMPropagation.ffrange(4, 1) == -0.5:0.25:0.25
    N = 123
    L = 321
    xrng = EMPropagation.ffrange(N, L)
    @test length(xrng) == N
    @test xrng[end] - xrng[1] == L - L / N

    dxs = 0.1
    xrng = -2:dxs:2
    dxv = fill(dxs, length(xrng))
    nf = @. exp(-xrng^2)
    @test approxeq(EMPropagation.beamwidth(dxs, nf), 2.4)
    @test approxeq(EMPropagation.beamwidth(dxs, nf), EMPropagation.beamwidth(dxv, nf))

    dxs = 0.1
    dys = 0.1
    xrng = -2:dxs:2
    yrng = -2:dys:2
    dxv = fill(dxs, length(xrng))
    dyv = fill(dys, length(yrng))
    nf = [exp(-x^2 - y^2) for x in xrng, y in yrng]
    @test approxeq(EMPropagation.beamwidth(dxs, dys, nf), 2.4)
    @test approxeq(
        EMPropagation.beamwidth(dxs, dys, nf),
        EMPropagation.beamwidth(dxv, dyv, nf),
    )
end
