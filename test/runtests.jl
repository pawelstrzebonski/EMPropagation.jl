import EMPropagation
import Test: @test_broken, @test, @test_throws, @testset

tests = ["utils", "nudft", "prop", "AutomaticPropagators"]

approxeq(a, b; rtol = 1e-6, atol = 0) = all(isapprox.(a, b, rtol = rtol, atol = atol))

for t in tests
    @info "Running " * t * ".jl"
    include("$(t).jl")
    @info "Finished " * t * ".jl"
end
