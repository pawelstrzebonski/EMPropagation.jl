@testset "prop.jl" begin
    #TODO: do proper tests

    # Run basic tests to see if there are any errors, not for correctness
    M, N = 20, 10
    u1D = ones(M)
    u2D = ones(M, N)
    dxs = 0.1
    dys = 0.2
    dxv = fill(dxs, M)
    dyv = fill(dys, N)
    lambda = 0.85
    z = 100
    f = x -> 0

    for (propfun, propfunfun) in zip(
        [
            EMPropagation.FraunhoferPropagator,
            EMPropagation.FresnelImpulseResponsePropagator,
            EMPropagation.FresnelTransferFunctionPropagator,
        ],
        [
            EMPropagation.FraunhoferPropagatorFun,
            EMPropagation.FresnelImpulseResponsePropagatorFun,
            EMPropagation.FresnelTransferFunctionPropagatorFun,
        ],
    )
        res = nothing
        @test (res = propfun(u1D, dxs, lambda, z); true)
        @test (f = propfunfun(u1D, dxs, lambda, z); true)
        @test isapprox(abs.(res[1]), abs.(f(u1D, z)[1]))
        @test (res = propfun(u1D, dxv, lambda, z); true)
        @test (f = propfunfun(u1D, dxv, lambda, z); true)
        @test isapprox(abs.(res[1]), abs.(f(u1D, z)[1]))
        @test (res = propfun(u2D, dxs, dys, lambda, z); true)
        @test (f = propfunfun(u2D, dxs, dys, lambda, z); true)
        @test isapprox(abs.(res[1]), abs.(f(u2D, z)[1]))
        @test (res = propfun(u2D, dxv, dyv, lambda, z); true)
        @test (f = propfunfun(u2D, dxv, dyv, lambda, z); true)
        @test isapprox(abs.(res[1]), abs.(f(u2D, z)[1]))
    end
end
