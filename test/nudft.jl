@testset "nudft.jl" begin
    import AbstractFFTs

    nf = ones(10)
    dxs = 0.1
    dxv = fill(dxs, length(nf))
    @test approxeq(EMPropagation.NUDFT(dxv, nf), AbstractFFTs.fft(nf), atol = 1e-5)

    nf = ones(10)
    dxs = 0.01
    dxv = fill(dxs, length(nf))
    @test approxeq(EMPropagation.NUDFT(dxv, nf), AbstractFFTs.fft(nf), atol = 1e-5)

    nf = ones(10, 10)
    dxs = 0.1
    dys = 0.1
    dxv = fill(dxs, size(nf, 1))
    dyv = fill(dys, size(nf, 2))
    @test approxeq(EMPropagation.NUDFT(dxv, dyv, nf), AbstractFFTs.fft(nf), atol = 1e-4)

    #TODO: NFFT seems to have some issues/inconsistency (see their github)

    nf = ones(10, 5)
    dxs = 0.01
    dys = 0.2
    dxv = fill(dxs, size(nf, 1))
    dyv = fill(dys, size(nf, 2))
    @test_broken EMPropagation.NUDFT(dxv, dyv, nf) == AbstractFFTs.fft(nf)
end
