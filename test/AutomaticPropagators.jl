@testset "AutomaticPropagators.jl" begin
    #TODO: do proper tests

    # Run basic tests to see if there are any errors, not for correctness
    M, N = 20, 10
    u1D = ones(M)
    u2D = ones(M, N)
    dxs = 0.1
    dys = 0.2
    dxv = fill(dxs, M)
    dyv = fill(dys, N)
    lambda = 0.85
    z = 100

    @test (EMPropagation.autofarfield(dxs, u1D, lambda, 10); true)
    @test (EMPropagation.autofarfield(dxv, u1D, lambda, 10); true)
    @test (EMPropagation.autofarfield(dxs, dys, u2D, lambda, 10); true)
    @test (EMPropagation.autofarfield(dxv, dyv, u2D, lambda, 10); true)

    # Rough test for propagator suggestor function
    #TODO: Do these suggestions make sense?

    nf = @. exp(-(-5:0.1:5)^2)
    @test EMPropagation.propagator_suggest(0.1, nf, 1, 1) ==
          EMPropagation.FresnelTransferFunctionPropagator
    @test EMPropagation.propagator_suggest(0.1, nf, 1, 10) ==
          EMPropagation.FresnelTransferFunctionPropagator
    @test EMPropagation.propagator_suggest(0.1, nf, 1, 100) ==
          EMPropagation.FraunhoferPropagator
    @test EMPropagation.propagator_suggest(10, nf, 1, 100000) ==
          EMPropagation.FresnelImpulseResponsePropagator
    @test EMPropagation.propagator_suggest(10, EMPropagation.zeropad(nf, 450), 1, 100000) ==
          EMPropagation.FresnelTransferFunctionPropagator
    @test EMPropagation.propagator_suggest(0.1, EMPropagation.zeropad(nf, 450), 1, 10) ==
          EMPropagation.FresnelTransferFunctionPropagator

    nf = [exp(-x^2 - y^2) for x = -5:0.1:5, y = -5:0.1:5]
    @test EMPropagation.propagator_suggest(0.1, 0.1, nf, 1, 1) ==
          EMPropagation.FresnelTransferFunctionPropagator
    @test EMPropagation.propagator_suggest(0.1, 0.1, nf, 1, 10) ==
          EMPropagation.FresnelTransferFunctionPropagator
    @test EMPropagation.propagator_suggest(0.1, 0.1, nf, 1, 100) ==
          EMPropagation.FraunhoferPropagator
    @test EMPropagation.propagator_suggest(10, 10, nf, 1, 100000) ==
          EMPropagation.FresnelImpulseResponsePropagator
    @test EMPropagation.propagator_suggest(
        10,
        10,
        EMPropagation.zeropad(nf, 450),
        1,
        100000,
    ) == EMPropagation.FresnelTransferFunctionPropagator
    @test EMPropagation.propagator_suggest(
        0.1,
        0.1,
        EMPropagation.zeropad(nf, 450),
        1,
        10,
    ) == EMPropagation.FresnelTransferFunctionPropagator
    @test EMPropagation.propagator_suggest(0.1, 10, nf, 1, 1) ==
          EMPropagation.FresnelTransferFunctionPropagator
    @test EMPropagation.propagator_suggest(10, 100, nf, 1, 100000) ==
          EMPropagation.FraunhoferPropagator
end
