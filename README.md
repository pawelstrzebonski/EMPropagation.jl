# EMPropagation

Documentation:
[![](https://img.shields.io/badge/docs-latest-blue.svg)](https://pawelstrzebonski.gitlab.io/EMPropagation.jl/)

## About

`EMPropagation` seeks to be a package for various complex electromagnetic
field propagation problems, especially near-field to far-field
propagation. It implements various efficient Fourier-based propagation
algorithms.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/EMPropagation.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for EMPropagation.jl](https://pawelstrzebonski.gitlab.io/EMPropagation.jl/).
