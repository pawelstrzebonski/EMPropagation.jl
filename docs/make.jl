using Documenter
import EMPropagation

makedocs(
    sitename = "EMPropagation.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "EMPropagation.jl"),
    pages = [
        "Home" => "index.md",
        "Examples and Usage" => [
            "Fresnel Transfer Function" => "fresneltf.md",
            "Fresnel Impulse Response" => "fresnelir.md",
            "Fraunhofer" => "fraunhoferff.md",
            "Near-Field to Far-Field Propagation" => "autofarfield.md",
            "Choosing a Propagator" => "choosing.md",
            "GPU Usage" => "gpu_example.md",
            "Performance Tips for Bulk Simulations" => "bulk_usage.md",
        ],
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "AutomaticPropagators.jl" => "AutomaticPropagators.md",
            "FraunhoferPropagator.jl" => "FraunhoferPropagator.md",
            "FresnelImpulseResponsePropagator.jl" => "FresnelImpulseResponsePropagator.md",
            "FresnelTransferFunctionPropagator.jl" => "FresnelTransferFunctionPropagator.md",
            "nudft.jl" => "nudft.md",
            "utils.jl" => "utils.md",
        ],
        "test/" => [
            "AutomaticPropagators.jl" => "AutomaticPropagators_test.md",
            "prop.jl" => "prop_test.md",
            "nudft.jl" => "nudft_test.md",
            "utils.jl" => "utils_test.md",
        ],
    ],
)
