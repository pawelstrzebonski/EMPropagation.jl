# GPU Usage

`EMPropagation` should be written in a GPU friendly fashion, so that
(most of) it will work with the standard
[Julia GPU packages](https://juliagpu.org/).
All that should be needed is to convert the function input arrays
from being standard CPU arrays to some form of
[GPUArrays](https://github.com/JuliaGPU/GPUArrays.jl).
While GPU compute support tends to be somewhat spotty in terms of hardware,
drivers, and software, your best bet on using a GPU with `EMPropagation`
is likely the
[CuArrays](https://github.com/JuliaGPU/CuArrays.jl)
package which support CUDA compatible NVIDIA GPUS. For example,
assuming that your basic inputs `dx`, `dy`, `u1`, `lambda0`, and `NZP` have been defined,
the following should be enough to do near-field to far-field
propagation on your GPU:

```
import EMPropagation
import CuArrays: cu
u1=cu(u1)
thetax, thetay, ff=EMPropagation.autofarfield(dx, dy, u1, lambda0, NZP)
```
