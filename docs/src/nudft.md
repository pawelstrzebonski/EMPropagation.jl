# Non-Uniform DFT

These functions wrap around the [`NFFT.jl`](https://github.com/tknopp/NFFT.jl)
package to enable us to calculate the FFT of non-uniformly sampled
fields.

---

```@autodocs
Modules = [EMPropagation]
Pages   = ["nudft.jl"]
```
