# AutomaticPropagators.jl

## Description

A basic set of tests to check whether the automatic propagator functions
run without error for a basic set of input parameters (1D and 2D fields,
uniform and non-uniform grids, `GPUArray` inputs, etc).
