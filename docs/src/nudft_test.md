# nudft.jl

## Description

A basic set of tests comparing the non-uniform FFT code to uniform FFT
code to check for (approximate) equivalence in the generate uniform
sampling case.
