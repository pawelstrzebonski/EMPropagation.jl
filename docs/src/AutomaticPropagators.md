# Automatic Propagators

These functions wrap around the propagator implementations themselves
and aim to provide simple interfaces that will automatically choose
the most appropriate algorithms and parameters for a given case.

---

```@autodocs
Modules = [EMPropagation]
Pages   = ["AutomaticPropagators.jl"]
```
