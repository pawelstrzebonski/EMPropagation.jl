# Performance Tips for Bulk Simulations

If you need to run many similar propagation simulations (simulations
that have the same input types and sizes, and only varying in the source
field and/or propagation distance), you may want to use the `XPropagatorFun`
versions of the `XPropagator` functions. Given an example of the problem
arguments, these functions will return a simplified/optimized propagator
function that takes a (similar size and type) field and propagation
distance as arguments. For example:

```Julia
# Import the package
import EMPropagation
# Define the problem parameters
dx1, u1, lambda, z = ...
# Create the optimized propagator function by calling the `XPropagatorFun` function
# that corresponds to the `XPropagator` function using the same arguments
# that you would otherwise
f = EMPropagation.FresnelTransferFunctionPropagatorFun(u1, dx1, lambda, z)
# Now you can use this function for faster simulations with different fields
# or distances
for z in zmin:dz:zmax
	# Run a propagation at a different distance
	u2, x2 = f(u1, z)
end
```

These `XPropagatorFun` should be largely equivalent to the `XPropagator`
functions (ie `FresnelTransferFunctionPropagator`,
`FresnelImpulseResponsePropagator`, and `FraunhoferPropagator`) in terms
of feature support and should return the same results. The degree of
performance improvement (whether allocations, memory usage, and run-time)
is highly dependent on the propagator algorithm and function arguments.
