# Fraunhofer Propagation

Let's consider the simple square beam example. We import the relevant
packages and calculate and plot the near-field:

```@example propFF
import EMPropagation
import Plots: plot, heatmap, scatter
import Plots: savefig # hide

L1=0.5
M=250
dx1=dy1=L1/M
y1=x1=-L1/2:dx1:L1/2-dx1
lambda=0.5e-6
k=2*pi/lambda
w=0.011
z=2000
u1=[abs(x)<=w && abs(y)<=w ? 1.0 : 0.0 for x=x1, y=y1]
heatmap(x1, y1, u1,
	xlabel="x",
	ylabel="y",
	size=(800, 800),
	aspectratio=1,
	title="Near-Field",
)
savefig("propFF_u1.png"); nothing # hide
```

![Square beam near-field](propFF_u1.png)

Now that we've calculated the near-field we can use the `FraunhoferPropagator`
function to calculate the far-field, which we then plot:

```@example propFF
u2, x2, y2=EMPropagation.FraunhoferPropagator(u1, dx1, dy1, lambda, z)
plot(
	heatmap(x2, y2, abs.(u2),
		xlabel="x",
		ylabel="y",
		title="Far-Field Magnitude",
		aspectratio=1,
	),
	scatter(x2, abs.(u2[:, div(size(u2, 1), 2)]),
		xlabel="x",
		ylabel="Far-Field Magnitude",
		legend=false,
	),
	size=(800, 400),
)
savefig("propFF_u2.png"); nothing # hide
```

![Far-field](propFF_u2.png)

We find that the above far-fields look sinc-like, as we would expect
(as the Fourier transform of a square function is a sinc function).
