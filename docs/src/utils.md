# Misc Utilities

Various backend functions and utilities.

---

```@autodocs
Modules = [EMPropagation]
Pages   = ["utils.jl"]
```
