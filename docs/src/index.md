# EMPropagation.jl

## About

`EMPropagation` seeks to be a package for various complex electromagnetic
field propagation problems, especially near-field to far-field
propagation. It implements various efficient Fourier-based propagation
algorithms that are described in
["Computational Fourier Optics: A MATLAB Tutorial" by David G. Voelz](https://doi.org/10.1117/3.858456).

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/EMPropagation.jl
```

## Status

* Basic propagation algorithms (Fresnel transfer function and impulse response, and Fraunhofer propagation) are implemented
* Supports both 1D and 2D fields
* Supports uniform and non-uniform sampling on the near-field (results not yet verified for non-uniform grid)
* Add wrapper function for quick-and-simple near-field to far-field propagation
* (Largely) GPU friendly using [GPUArrays](https://github.com/JuliaGPU/GPUArrays.jl) for uniformly sampled fields (compatibility tested against `GPUArrays.JLArrays` backend)
