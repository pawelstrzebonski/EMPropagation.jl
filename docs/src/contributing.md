# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Optimizations (especially in terms of allocation)
* Write and link to beam evaluation utilities (separate package)
* Enforce code consistency and decrease repetition
* Verify non-uniform near-field grid function
* Add wrapper function for automatically choosing best propagator code, making necessary parameter choices
* 2-step propagation functions? Other relevant utilities?
* Other propagation algorithms?

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* We aim to support both 1D and 2D fields
* We aim to support both uniformly and non-uniformly sampled grids (at least in the near-field)
* We aim to provide seamless GPU compute support via `GPUArrays`
* We aim to minimize the amount of code and function repetition when implementing the above features
