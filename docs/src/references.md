# References

This package is heavily influenced by
["Computational Fourier Optics: A MATLAB Tutorial" by David G. Voelz](https://doi.org/10.1117/3.858456).
This book covers many aspects of computational Fourier optics, ranging
from the theory to the (MATLAB) implementation. Of particular
relevance to this package is chapter 5 which provides example MATLAB
code for uniform grid 2D propagator functions and discusses the various
aspects of the propagation problems as relevant to the practical
implementation (in particular sampling and bandwidth issues).

It is highly recommended to read this book (especially
chapter 5) to ensure that one correctly uses the algorithms
implemented in this package.
