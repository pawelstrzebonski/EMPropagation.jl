# Fresnel Transfer Function Propagation

Let's consider the simple square beam example. We import the relevant
packages and calculate and plot the near-field:

```@example propIR
import EMPropagation
import Plots: plot, heatmap
import Plots: savefig # hide

L1=0.5
M=250
dx1=dy1=L1/M
y1=x1=-L1/2:dx1:L1/2-dx1
lambda=0.5e-6
k=2*pi/lambda
w=0.051
z=2000
u1=[abs(x)<=w && abs(y)<=w ? 1.0 : 0.0 for x=x1, y=y1]
heatmap(x1, y1, u1,
	xlabel="x",
	ylabel="y",
	size=(800, 800),
	aspectratio=1,
	title="Near-Field",
)
savefig("propIR_u1.png"); nothing # hide
```

![Square beam near-field](propIR_u1.png)

Now that we've calculated the near-field we can use the `FresnelImpulseResponsePropagator`
function to calculate the propagated field, which we then plot:

```@example propIR
u2, x2, y2=EMPropagation.FresnelImpulseResponsePropagator(u1, dx1, dy1, lambda, z)
plot(
	heatmap(x2, y2, abs2.(u2),
		xlabel="x",
		ylabel="y",
		title="Propagated Field intensity",
		aspectratio=1,
	),
	plot(x2, abs2.(u2[:, div(size(u2, 1), 2)]),
		xlabel="x",
		ylabel="Propagated Field intensity",
		legend=false,
	),
	size=(800, 400),
)
savefig("propIR_u2.png"); nothing # hide
```

![Propagated field intensity](propIR_u2.png)

Instead of plotting the field intensity, we can plot the magnitude
and phase:

```@example propIR
import DSP
plot(
	plot(x2, abs.(u2[:, div(size(u2, 1), 2)]),
		xlabel="x",
		ylabel="Propagated Field Magnitude",
		legend=false,
	),
	plot(x2, DSP.unwrap(angle.(u2[:, div(size(u2, 1), 2)])),
		xlabel="x",
		ylabel="Propagated Field Phase",
		legend=false,
	),
	size=(800, 400),
)
savefig("propIR_AP.svg"); nothing # hide
```

![Propagated field magnitude and phase](propIR_AP.svg)
