# Near-Field to Far-Field Propagation

If you just have a near-field and simply want a far-field, then the
`autofarfield` function is for you. `autofarfield` will use Fraunhofer
propagation, choosing z such that the algorithm is in the critical sampling
regime, and will return the resulting far-field angular coordinates.

As an example, we will continue to use the square beam example we've been
using before. We calculate the near-field:

```@example autofarfield
import EMPropagation
import Plots: plot, heatmap
import Plots: savefig # hide

L1=0.5
M=250
dx1=dy1=L1/M
y1=x1=-L1/2:dx1:L1/2-dx1
lambda=0.5e-6
k=2*pi/lambda
w=0.051
z=2000
u1=[abs(x)<=w && abs(y)<=w ? 1.0 : 0.0 for x=x1, y=y1]
heatmap(x1, y1, u1,
	xlabel="x",
	ylabel="y",
	size=(800, 800),
	aspectratio=1,
	title="Near-Field",
)
savefig("autofarfield_u1.png"); nothing # hide
```

![Square beam near-field](autofarfield_u1.png)

And then we propagate it to the far-field:

```@example autofarfield
tx, ty, u2=EMPropagation.autofarfield(dx1, dy1, u1, lambda, 0)
plot(
	heatmap(tx, ty, abs2.(u2),
		xlabel="theta_x",
		ylabel="theta_y",
		title="Far-Field Intensity",
		aspectratio=1,
	),
	size=(800, 800),
)
savefig("autofarfield_u2.png"); nothing # hide
```

![Far-field intensity](autofarfield_u2.png)

The above image is too zoomed-out and grainy to be particularly
useful! Never mind though. The (last) `NZP` argument to `autofarfield` which
was set to 0 determines the amount of zero-padding to be applied to the
near-field before propagating. Increasing the zero-padding will increase
the far-field sampling precision, at the cost of higher memory and CPU
time usage. So we repeat the propagation with `NZP=500` and we zoom in
on the center of the far-field:

```@example autofarfield
tx, ty, u2=EMPropagation.autofarfield(dx1, dy1, u1, lambda, 500)
xidx, yidx=abs.(tx).<3e-5, abs.(ty).<3e-5
tx, ty, u2=tx[xidx], ty[yidx], u2[xidx, yidx]
plot(
	heatmap(tx, ty, abs2.(u2),
		xlabel="theta_x",
		ylabel="theta_y",
		title="Far-Field Intensity",
		aspectratio=1,
	),
	size=(800, 800),
)
savefig("autofarfield_u2_NZP.png"); nothing # hide
```

![Far-field intensity](autofarfield_u2_NZP.png)
