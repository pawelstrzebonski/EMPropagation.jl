# Choosing a Propagator

There are three propagation algorithms implemented and provided in this
package: Fresnel Transfer Function, Fresnel Impulse Response, and
Fraunhofer propagation. How well each of these performs is dependent
on various parameters, including the near-field sampling ``\Delta x``,
wavelength ``\lambda``, near-field size L and propagation distance z.
Which one will work best for you?

If all you have is a near-field and you just want to know the far-field,
then you probably are best using the `autofarfield` function which
will use the computationally most efficient Fraunhofer algorithm.
However, one must also ensure that a sufficient number of grid
samples is provided if the far-field phase is to be correct
(this can be increased via zero-padding, as set by
the `NZP` argument).

Please consult Chapter 5 (especially Section 5.4)
of the "Computational Fourier Optics" reference for an explanation
for choosing the various parameter and algorithms. A function to
auto-magically choose the correct parameters is on the TODO list,
and any help implementing it would be appreciated. At the moment,
the work-in-progress `propagator_suggest` function will analyze the input
field for the various criteria and make comments/warnings/suggestions.
However, it is *not* a replacement for a good understanding of the
problem and propagation methods.
